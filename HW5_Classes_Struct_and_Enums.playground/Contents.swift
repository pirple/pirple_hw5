 import UIKit

/*
Create a class called "Vehicle" and add methods that allow you to set the "Make", "Model", "Year,", and "Weight".
The class should also contain a "NeedsMaintenance" boolean that defaults to False, and and "TripsSinceMaintenance" Integer that defaults to 0.

Next create a subclass that inherits the properties of Vehicle class. Call this new subclass "Cars".

The Cars class should contain a method called "Drive" that sets the state of a boolean isDriving to True.  It should have another method called "Stop" that sets the value of isDriving to false.

Switching isDriving from true to false should increment the "TripsSinceMaintenance" counter. And when TripsSinceMaintenance exceeds 100, then the NeedsMaintenance boolean should be set to true.

Add a "Repair" method to either class that resets the TripsSinceMaintenance to zero, and NeedsMaintenance to false.

Create 3 different cars, using your Cars class, and drive them all a different number of times. Then print out their values for Make, Model, Year, Weight, NeedsMaintenance, and TripsSinceMaintenance

Extra Credit:

Create a Planes class that is also a subclass of Vehicle. Add methods to the Planes class for Flying and Landing (similar to Driving and Stopping), but different in one respect: Once the NeedsMaintenance boolean gets set to true, any attempt at flight should be rejected (return false), and an error message should be printed saying that the plane can't fly until it's repaired.
*/


class Vechicle {
    var make : String;
    var model : String;
    var year : Int;
    var weight : Int; // in lbs
    var needsMaintenance : Bool = false;
    var tripsSinceMaintenance : Int = 0

    // default init:
    init() {
        self.make = "";
        self.model = "";
        self.year = 0;
        self.weight = 0;
    }

    // init to add all at once:
    init(_make: String, _model: String, _year: Int, _weight: Int) {
        self.make = _make;
        self.model = _model;
        self.year = _year;
        self.weight = _weight;
    }
    
    
    func setMake(_make: String){
        self.make = _make;
    }
    
    func setModel(_model: String){
        self.model = _model;
    }
    
    func setYear(_year: Int){
        self.year = _year;
    }
    
    func setWeight(_weight: Int){
        self.weight = _weight;
    }
    
    func setNeedsMaintenance(_needsMaintenance: Bool){
        self.needsMaintenance = _needsMaintenance;
    }
    
    func setTripsSinceMaintenance(_tripsSinceMaintenance: Int){
        self.tripsSinceMaintenance = _tripsSinceMaintenance;
    }
    
 }


class Cars : Vechicle {
    var isDriving : Bool = true;
    
    func Drive()  {
        
        if (super.tripsSinceMaintenance == 101 ){ // only setting needsMaintenance ones and print warning message once
            super.needsMaintenance = true;
            print("\(super.make), \(super.model) : Warning ---> Maintenance is needed for your car.")
        }
        self.isDriving = true;
    }
    
    func Stop(){
        self.isDriving = false;
        super.tripsSinceMaintenance += 1;
    }
    
    func repair() {
        super.tripsSinceMaintenance = 0;
        super.needsMaintenance = false;
        print("\(super.make), \(super.model) : Your car have been reparied and it is as good as new.");
    }
    
    func printCarDetails() {
        print("""
        
        Car Details:
        ============================================================
        Make   ..................: \(super.make)
        Model  ..................: \(super.model)
        Year   ..................: \(super.year)
        Weight ..................: \(super.weight)
        Needs Maintenance........: \(super.needsMaintenance)
        Trips Since Maintenance .: \(super.tripsSinceMaintenance)
        Is Driving ..............: \(self.isDriving)
        ============================================================
        
        """)
    }
 }


class Planes : Vechicle {
    var isFlying : Bool = true;
    
    func fly()  {
        if (super.tripsSinceMaintenance > 99 ){
            super.needsMaintenance = true;
            print("\(super.make), \(super.model) : ERROR ---> Maintenance is needed in order to fly.")
        } else {
            self.isFlying = true;
        }
    }
    
    func land(){
        self.isFlying = false;
        super.tripsSinceMaintenance += 1;
    }
    
    func repair() {
        super.tripsSinceMaintenance = 0;
        super.needsMaintenance = false;
        print("\(super.make), \(super.model) : Your Plane have been reparied and it is as good as new.");
    }
 }

/*
                                    CAR PART
*/
// using init to input values all together:
var kia : Cars = Cars(_make: "Kia", _model: "Optima", _year: 2014, _weight: 3230);

// using methods to input values:
var honda : Cars = Cars();
honda.setMake(_make: "Honda");
honda.setModel(_model: "CR-V");
honda.setYear(_year: 2016);
honda.setWeight(_weight: 3512)

var bmw : Cars = Cars(_make: "BMW", _model: "3 Series", _year: 2019, _weight: 4010);

// Trips for the 3 cars:
for numbers in 1...160 {
    
    if (numbers % 2 == 0) {
        honda.Drive();
        bmw.Drive();
        bmw.Stop()
    } else {
        bmw.Drive();
        bmw.Stop()
        honda.Stop();
    }
    
    kia.Drive();
    kia.Stop();
    
 }
 
 // repairing a car:
 if (kia.needsMaintenance == true) {
    kia.repair();
 }
 

kia.printCarDetails();
honda.printCarDetails();
bmw.printCarDetails();



/*
                                EXTRA CREDIT PLANES CLASS:
*/

print("""
Plane: 
======================================================================
""")

var plane1 : Planes = Planes(_make: "Boeing", _model: "737", _year: 2015, _weight: 174200);

for numbers in 1...120 {
    
    if (numbers == 104) { // reapiring plane after 3 attempts of flying
        plane1.tripsSinceMaintenance
        plane1.repair();
    } else {
        plane1.fly();
        plane1.land();
    }
    
 }


print("""
======================================================================
""")



